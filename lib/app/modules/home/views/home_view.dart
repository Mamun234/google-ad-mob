import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('HomeView'),
          centerTitle: true,
        ),
        body: GetBuilder<HomeController>(builder: (_) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              controller.bannerAd != null
                  ? Align(
                      alignment: Alignment.bottomCenter,
                      child: SafeArea(
                        child: SizedBox(
                          width: controller.bannerAd!.size.width.toDouble(),
                          height: controller.bannerAd!.size.height.toDouble(),
                          child: AdWidget(ad: controller.bannerAd!),
                        ),
                      ),
                    )
                  : Container(),
              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                  onPressed: () {
                    controller.displayBannerAd();
                  },
                  child: const Text(
                    "Display a banner ad",
                    style: TextStyle(fontSize: 16),
                  )),

              //===========interstitial==========
              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                  onPressed: () {
                    controller.displayInterstitial();
                  },
                  child: const Text(
                    "Display an interstitial ad",
                    style: TextStyle(fontSize: 16),
                  )),

              //============displayNativeAd========

              const SizedBox(
                height: 30,
              ),

              controller.nativeAd == null
                  ? Container()
                  : ConstrainedBox(
                      constraints: const BoxConstraints(
                        minWidth: 320, // minimum recommended width
                        minHeight: 90, // minimum recommended height
                        maxWidth: 400,
                        maxHeight: 200,
                      ),
                      child: AdWidget(ad: controller.nativeAd!),
                    ),
              ElevatedButton(
                  onPressed: () {
                    controller.displayNativeAd();
                  },
                  child: const Text(
                    "Display an native ad",
                    style: TextStyle(fontSize: 16),
                  )),

              //============Rewarded=========

              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                  onPressed: () {
                    controller.displayRewardedAd();
                  },
                  child: const Text(
                    "Display an rewarded ad",
                    style: TextStyle(fontSize: 16),
                  )),

              //============Rewarded Interstitial=========

              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                  onPressed: () {
                    controller.displayRewardedInterstitialAd();
                  },
                  child: const Text(
                    "Display an rewarded Interstitial ad",
                    style: TextStyle(fontSize: 16),
                  )),

              //============AppOpenAd=========
              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                  onPressed: () {
                    controller.displayAppOpenAds();
                  },
                  child: const Text(
                    "Display an AppOpen Ad",
                    style: TextStyle(fontSize: 16),
                  )),
            ],
          );
        }));
  }
}
